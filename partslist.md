| Part Name | Price (INR) |
|:--------------:|:---------:|
| 1. [Jumper Cables](https://robu.in/product/20cm-dupont-wire-color-jumper-cable-2-54mm-1p-1p-male-to-male-40pcs/) | 70.00 |
| 2. [Soldering Kit](https://www.amazon.in/APTECH-DEALS-AP-SI11A010-Soldering-Intermediate/dp/B07KJ9HS2F/ref=pd_sbs_328_5/259-6853203-5139823?_encoding=UTF8&pd_rd_i=B07KJ9HS2F&pd_rd_r=3d35df0e-7ae9-11e9-b566-510bb4696bcb&pd_rd_w=O5fWK&pd_rd_wg=jAK32&pf_rd_p=87667aae-831c-4952-ab47-0ae2a4d747da&pf_rd_r=242CG95RAE6SC9GYK42X&psc=1&refRID=242CG95RAE6SC9GYK42X) | 299.00 |
| 3. [Multimeter](https://www.amazon.in/Hao-Yue-D830D-Multimeter-professional/dp/B00MKBFKIE/ref=pd_sbs_328_10?_encoding=UTF8&pd_rd_i=B00MKBFKIE&pd_rd_r=4cab95ad-7aea-11e9-9794-6b186da2a7c7&pd_rd_w=p94pj&pd_rd_wg=sLJxY&pf_rd_p=87667aae-831c-4952-ab47-0ae2a4d747da&pf_rd_r=Z01VCW2M3TW54B2MP90G&psc=1&refRID=Z01VCW2M3TW54B2MP90G) | 249.00 |
| 4. [XCLUMA Bluetooth 4.0 CSR8645 Amplifier Board APT-X Stereo Receiver Amp Module](https://www.amazon.in/XCLUMA-Bluetooth-CSR8645-Amplifier-Receiver/dp/B075YSHN92/ref=sr_1_1?keywords=csr8645&qid=1558346516&s=gateway&sr=8-1) | 945.00 |
| 5. [eHUB Micro USB 5V 1A 18650 TP4056 Lithium Battery Charging Module Board with Protection](https://www.amazon.in/eHUB-Lithium-Battery-Charging-Protection/dp/B07FW6XDTB/ref=pd_bxgy_23_2/259-6853203-5139823?_encoding=UTF8&pd_rd_i=B07FW6XDTB&pd_rd_r=ac0f6f63-7ae6-11e9-9b14-ad0a6ddaf222&pd_rd_w=N713g&pd_rd_wg=TjEDR&pf_rd_p=92c5dc59-6da4-488e-ba00-262537223f6b&pf_rd_r=53S5K5HSYG7NMJZQQM99&psc=1&refRID=53S5K5HSYG7NMJZQQM99) | 120.00 |
| 6. [Rechargable Lithium ion battery](https://www.amazon.in/Generic-Schrodinger15-Lithium-70039Rechargeable-Battery/dp/B07KK74N78/ref=sr_1_2?keywords=lithium+rechargeable+battery&qid=1558346670&s=gateway&sr=8-2) | 340.00 |
| 7. [Glue gun](https://www.amazon.in/THEMISTO-passion-Sticks-Heating-Transparent/dp/B07GQVJL5K/ref=cm_cr_srp_d_product_top?ie=UTF8) | 300.00 |
| Total | 2323.00 |
